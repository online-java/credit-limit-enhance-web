package com.rmsbank.cards.offers.web.config;

import java.io.InputStream;
import java.util.Properties;

import com.google.common.io.Resources;


public class ApplicationPropertiesConfig {
	static Properties properties;
	static {
		properties = new Properties();

	}

	public static  Properties getEnvironmentProperties() {
		if (properties == null) {
			properties = new Properties();
		}

		try {
			String env = System.getProperty("env");
			
			System.out.println(env);
			InputStream props = Resources.getResource("application-"+env+".properties").openStream();
			properties.load(props);

		} catch (Exception e) {

		}

		return properties;
	}

}
