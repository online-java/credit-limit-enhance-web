package com.rmsbank.cards.offers.service;

import com.google.gson.Gson;
import com.rmsbank.cards.offers.beans.ClePromocodeServiceResponse;
import com.rmsbank.cards.offers.exception.BusinessException;
import com.rmsbank.cards.offers.web.config.ApplicationPropertiesConfig;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

public class CreditLimitEnhnaceServiceImpl implements CreditLimitEnhanceService {

	@Override
	public ClePromocodeServiceResponse verifyPromocode(String promocode, String access_token) throws BusinessException {

		HttpResponse<String> accessedApiResponse = Unirest
				.get(ApplicationPropertiesConfig.getEnvironmentProperties().getProperty("resource.endpoint.url")+promocode)
				.header("content-type", "application/json").header("authorization", "Bearer " + access_token)
				.asString();

		System.out.println("unirest response......................" + accessedApiResponse.getBody().toString());
		
		String httpResponse =accessedApiResponse.getBody();
		Gson gson = new Gson();
		ClePromocodeServiceResponse clePromocodeServiceResponse=gson.fromJson(httpResponse, ClePromocodeServiceResponse.class);
		
		System.out.println("response..............."+clePromocodeServiceResponse.toString());

		/*
		 * Client cleRestClient = Client.create();
		 * 
		 * 
		 * String verifyPromocodeUrl =
		 * "http://localhost:8080/credit-limit-enhancemennt/api/cle-limit/promocode";
		 * WebResource webResource2 = cleRestClient.resource(verifyPromocodeUrl);
		 * 
		 * 
		 * ClientResponse response = webResource2.queryParam("promocode",
		 * "hdbank101").accept(MediaType.APPLICATION_XML) .get(ClientResponse.class);
		 * 
		 * 
		 * CreditLimitEnhanceResponse ehanceResponse =(CreditLimitEnhanceResponse)
		 * response.getEntity(CreditLimitEnhanceResponse.class);
		 * 
		 * System.out.println(ehanceResponse);
		 * 
		 * 
		 * webResource2.queryParam("promocode", "hdbank101")
		 * .accept(MediaType.APPLICATION_XML).get(CreditLimitEnhanceResponse.class);
		 * 
		 * String crditLimitResponseString = null; int status;
		 */ 
		return clePromocodeServiceResponse;

	}

	public static void main(String[] args) {

			}

}
