package com.rmsbank.cards.offers.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.rmsbank.cards.offers.beans.ClePromocodeServiceResponse;
import com.rmsbank.cards.offers.exception.BusinessException;
import com.rmsbank.cards.offers.form.CleRequestForm;
import com.rmsbank.cards.offers.form.RedeemForm;
import com.rmsbank.cards.offers.service.CreditLimitEnhanceService;
import com.rmsbank.cards.offers.service.CreditLimitEnhnaceServiceImpl;

@Controller
public class CreditLimitEhanceController {
	CreditLimitEnhanceService cleService = new CreditLimitEnhnaceServiceImpl();

	@RequestMapping(value = "/enhance", method = RequestMethod.GET)
	public String index() {
		return "crditlimitehnace";
	}

	@RequestMapping(value = "/verify", method = RequestMethod.POST)
	public ModelAndView enhanceLimit(@ModelAttribute CleRequestForm cleRequestForm, HttpSession session) {

		System.out.println("sessin value" + session.getAttribute("ACCESS_TOKEN"));
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("success");
		System.out.println(cleRequestForm.getPromocode());

		try {
			ClePromocodeServiceResponse creditLimitSvcResp = cleService.verifyPromocode(cleRequestForm.getPromocode(),
					(String) session.getAttribute("ACCESS_TOKEN"));

			// updated the customer details table

			// then return scces.jsp modelAndView.setViewName("success"); return
			modelAndView.setViewName("redemption");
			modelAndView.addObject("svcRsp", creditLimitSvcResp);
			return modelAndView;

		} catch (BusinessException e) {
			modelAndView.setViewName("error");

			System.out.println("in conntoller" + e);
			modelAndView.addObject("error", e);
		}

		return modelAndView;
	}

	@RequestMapping(value = "/reedeem", method = RequestMethod.POST)
	public ModelAndView redeemProcode(@ModelAttribute RedeemForm redeemForm) {
		System.out.println(redeemForm);
		// forward the request service layer
		// we call one more web service...............................
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("success");
		return modelAndView;
	}

}
