package com.rmsbank.cards.offers.controller;

import java.io.IOException;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rmsbank.cards.offers.web.config.ApplicationPropertiesConfig;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
/**
 * Created by JavaDeveloperZone on 19-07-2017.
 */
@Controller
public class CleLoginController {

	@GetMapping("/login")
	public ModelAndView getForm(@RequestParam("code") String code) throws IOException {
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>> code received " + code);

		String credentials = "myclient:secret";
		String encodedCredentials = new String(Base64.encodeBase64(credentials.getBytes()));

		System.out.println("access token url..."+ApplicationPropertiesConfig.getEnvironmentProperties().getProperty("access.token.url"));
		HttpResponse<String> response = Unirest.post(ApplicationPropertiesConfig.getEnvironmentProperties().getProperty("access.token.url"))
				.header("content-type", "application/x-www-form-urlencoded")
				.header("Authorization", "Basic " + encodedCredentials)
				.body("grant_type=authorization_code&client_id=myclient&client_secret=secret&code=" + code
						+ "&redirect_uri=http://localhost:6060/credit-limit-enhance-web/login")
				.asString();
		System.out.println("reponse body"+response.getBody());
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(response.getBody());
		String ACCESS_TOKEN = node.path("access_token").asText();
		System.out.println(ACCESS_TOKEN);
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("home");
		modelAndView.addObject("ACCESS_TOKEN", ACCESS_TOKEN);

		
		 
		return modelAndView;
	}

	public static void main(String[] args) {
		String code = "JNP0FK";
		String credentials = "myclient:secret";
		String encodedCredentials = new String(Base64.encodeBase64(credentials.getBytes()));

		HttpResponse<String> response = Unirest.post("http://localhost:9999/oauth/token")
				.header("content-type", "application/x-www-form-urlencoded")
				.header("Authorization", "Basic " + encodedCredentials)
				.body("grant_type=authorization_code&client_id=myclient&client_secret=secret&code=" + code
						+ "&redirect_uri=http://localhost:8079/getForm")
				.asString();
		System.out.println(response.getBody());

	}

}
