package com.rmsbank.cards.offers.form;

public class RedeemForm {

	private Double currentLimit;
	private Double eligibleAmount;
	private String expdate;
	private String promocode;
	@Override
	public String toString() {
		return "RedeemForm [currentLimit=" + currentLimit + ", eligibleAmount=" + eligibleAmount + ", expdate="
				+ expdate + ", promocode=" + promocode + "]";
	}
	public Double getCurrentLimit() {
		return currentLimit;
	}
	public void setCurrentLimit(Double currentLimit) {
		this.currentLimit = currentLimit;
	}
	public Double getEligibleAmount() {
		return eligibleAmount;
	}
	public void setEligibleAmount(Double eligibleAmount) {
		this.eligibleAmount = eligibleAmount;
	}
	public String getExpdate() {
		return expdate;
	}
	public void setExpdate(String expdate) {
		this.expdate = expdate;
	}
	public String getPromocode() {
		return promocode;
	}
	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}

	

}
