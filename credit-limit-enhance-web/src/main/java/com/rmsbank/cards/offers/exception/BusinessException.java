package com.rmsbank.cards.offers.exception;

public class BusinessException extends Exception {
	private String errorMessage;
	private String errorCode;

	public BusinessException(String errorMessage, String errorCode) {
		super();
		this.errorMessage = errorMessage;
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}

	public String getErrorCode() {
		return this.errorCode;
	}

	@Override
	public String toString() {
		return "BusinessException [errorMessage=" + errorMessage + ", errorCode=" + errorCode + "]";
	}

	
}
