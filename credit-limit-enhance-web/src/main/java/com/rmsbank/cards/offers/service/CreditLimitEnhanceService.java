package com.rmsbank.cards.offers.service;


import com.rmsbank.cards.offers.beans.ClePromocodeServiceResponse;
import com.rmsbank.cards.offers.exception.BusinessException;

public interface CreditLimitEnhanceService {
	
	public ClePromocodeServiceResponse verifyPromocode(String promocode,String access_token) throws BusinessException;

	

}
